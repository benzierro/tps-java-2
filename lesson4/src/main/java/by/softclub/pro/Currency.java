package by.softclub.pro;

public enum Currency {
    BYN,
    USD,
    EURO,
    RUB
}
