package by.softclub.pro;

public class RubConverter implements Converter {
    @Override
    public double convert(double price) {
        return price * 2;
    }
    @Override
    public Currency getCurrency() {
        return Currency.RUB;
    }
}
