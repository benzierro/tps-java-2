package by.softclub.pro;

public class EuroConverter implements Converter {
    @Override
    public double convert(double price) {
        return price * 4;
    }
    @Override
    public Currency getCurrency() {
        return Currency.EURO;
    }
}
