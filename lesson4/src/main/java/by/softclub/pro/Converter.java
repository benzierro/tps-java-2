package by.softclub.pro;

public interface Converter {
    public double convert(double price);
    public Currency getCurrency();
}
