package by.softclub.pro;

import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CarInfo {
    public static void main(String[] args) {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext(
                "applicationContext.xml"
        );

        CurrencyExchange currencyExchange = context.getBean("currencyExchange", CurrencyExchange.class);

        double res = currencyExchange.convert(Currency.valueOf(args[0]), Car.valueOf(args[1]).getPrice());

        if (args.length == 3) {
            try (BufferedWriter writer = Files.newBufferedWriter(Paths.get(args[2]), StandardCharsets.UTF_8))
            {
                writer.write(String.valueOf(res));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println(res);
        }

        context.close();
    }
}
