package by.softclub.pro;

public class UsdConverter implements Converter {
    @Override
    public double convert(double price) {
        return price * 3;
    }
    @Override
    public Currency getCurrency() {
        return Currency.USD;
    }
}
