package by.softclub.pro;

public enum Car {
    BMW (10000),
    AUDI (20000);

    public double getPrice() {
        return price;
    }

    private double price;

    Car(double price) {
        this.price = price;
    }
}
