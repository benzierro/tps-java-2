package by.softclub.pro;

import java.util.HashMap;
import java.util.List;

public class CurrencyExchange {
    List<Converter> converters;

    public CurrencyExchange(List<Converter> converters) {
        this.converters = converters;
    }

    public double convert(Currency currency, double price) {
        double res = 0;
        for (Converter converter : converters
        ) {
            if (converter.getCurrency() == currency) {
                res = converter.convert(price);
            }
        }
        return res;
    }

}
